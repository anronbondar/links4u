import './page.scss';
import Header from './sections/header/header';
import Hyperlinks from './sections/hyperlinks/hyperlinks';
import Plans from './sections/plans/plans';
import Footer from './sections/footer/footer';

function Page() {
    return (
        <>
            <Header/>
            <Hyperlinks/>
            <Plans/>
            <Footer/>
        </>
    );
}

export default Page;
