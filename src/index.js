import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import './ic_fonts/fonts.css';
import Page from './page';

const root = ReactDOM.createRoot(document.getElementById('wrapper'));
root.render(
  <React.StrictMode>
    <Page />
  </React.StrictMode>
);