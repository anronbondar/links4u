import './button.scss';

function Button({buttonClass, buttonModify, buttonValue, isWhite}) {
    return (
        <button
            className={`${buttonClass} button ${buttonModify} ${isWhite ? 'button_is-white' : ''} default-text`}>{buttonValue}
        </button>
    );
}

export default Button;
