import './link.scss';

function Link({linkClass, linkModify, linkHref, linkValue}) {
    return (
        <a
            className={`${linkClass ? `${linkClass}` : ''} link default-text ${linkModify ? `${linkModify}` : ''}`}
            href={linkHref}
        >
            {linkValue}
        </a>
    );
}

export default Link;
