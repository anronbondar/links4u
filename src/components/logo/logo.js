import './logo.scss';
import Link from '../link/link';
import logo from '../../img/logo.svg';
import logo_dark from '../../img/logo_dark.svg';

function Logo({isDark}) {

    let logoSrc = isDark ? logo_dark : logo;

    return (
        <Link
            linkHref={'/'}
            linkValue={<img src={logoSrc} alt={'logo'}/>}
        />
    );
}

export default Logo;
