import './plan-title.scss';

function PlanTitle({isWhite, planTitleText}) {
  return (
      <h3 className={`plan-title ${isWhite? 'plan-title_white' : ''}`}>{planTitleText}</h3>
  );
}

export default PlanTitle;
