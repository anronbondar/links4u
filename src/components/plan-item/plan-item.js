import './plan-item.scss';
import PlanTitle from '../plan-title/plan-title';
import Subtitle from '../subtitle/subtitle';
import Button from '../button/button';

function PlanItem({planItemInfo, isWhite}) {
    return (
        <div className={`plan-item ${isWhite ? 'plan-item_white' : ''}`}>
            {planItemInfo.popular ? <span>POPULAR</span> : null}
            <div className='plan-item__row'>
                <PlanTitle isWhite={isWhite} planTitleText={planItemInfo.title}/>
                <Subtitle subtitleClass={`plan-item__subtitle ${isWhite ? 'plan-item__subtitle_white' : ''}`}
                          subtitleText={planItemInfo.subtitle}
                />
                <div className='plan-item__price-line'>
                    <span className='plan-item__price-value'>${planItemInfo.price}</span>
                    <span className='plan-item__price-info'>Per member, per Month</span>
                </div>
                <Button isWhite={isWhite} buttonClass={'button__plan-item'} buttonModify={'button_small-rad'}
                        buttonValue={'Start free 14-day Trial'}
                />
                <span>No credit card required</span>
            </div>
        </div>
    );
}

export default PlanItem;
