import './navigation.scss';
import Logo from '../logo/logo';
import Link from '../link/link';
import Button from '../button/button';

function Navigation() {
    return (
        <div className='header__navigation'>
            <Logo isDark={false}/>
            <div className='header__nav'>
                <ul>
                    <li>
                        <Link linkHref={'/'} linkClass={'header__link'} linkValue={'Home'}
                              linkModify={'header__link_active'}/>
                    </li>
                    <li>
                        <Link linkHref={'#prices'} linkClass={'header__link'} linkValue={'Prices'}/>
                    </li>
                    <li>
                        <Link linkHref={'#contact_us'} linkClass={'header__link'} linkValue={'Contact Us'}/>
                    </li>
                    <li>
                        <Link linkHref={'#sign_in'} linkClass={'header__link'} linkValue={'Sign In'}/>
                    </li>
                </ul>
                <div>
                    <Button buttonClass={'header__nav-button'} buttonValue={'Sign UP'}/>
                </div>
            </div>
            <div className="header__burger">
                <span></span>
            </div>
        </div>
    );
}

export default Navigation;
