import './paragraph.scss';

function Paragraph({pClass, pText}) {

    let text = pText === 1 ?
        'They will crawl the links between the individual pages on your website, and they will crawl the links between entire websites'
        :
        pText === 2 ?
            'A hyperlink (usually just called a link) is a way for users to navigate between pages on the Internet. Search engines use links to crawl the web. They will crawl the links between the individual pages on your website, and they will crawl the links between entire websites.'
            :
            'Once search engines have crawled pages on the web, they can extract the content of those pages and add it to their indexes. In this way, they can decide if they feel a page is of sufficient quality to be ranked well for relevant keywords.'
    ;

    return (
        <p className={`${pClass} paragraph`}>{text}</p>
    );
}

export default Paragraph;
