import './subtitle.scss';

function Subtitle({subtitleClass, subtitleText}) {
    return (
        <h5 className={`${subtitleClass} subtitle`}>{subtitleText}</h5>
    );
}

export default Subtitle;
