import './checkbox.scss';

function Checkbox() {
  return (
      <label className="checkbox">
          <input type="checkbox" />
          <span className="checkbox-body"></span>
      </label>
  );
}

export default Checkbox;
