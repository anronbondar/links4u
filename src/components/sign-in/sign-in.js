import './sign-in.scss';
import Title from '../title/title';
import Input from '../input/input';
import Checkbox from '../checkbox/checkbox';
import Button from "../button/button";
import Link from '../link/link';

function SignIn() {
    return (
        <div className='sign-in'>
            <Title titleText={'Sign Up Now'}/>
            <div className='sign-in__inputs'>
                <Input typeValue={'name'} placeholderValue={'Your name'} isItalic={true}/>
                <Input typeValue={'surname'} placeholderValue={'Your surname'} isItalic={true}/>
                <Input typeValue={'email'} placeholderValue={'Your email'} isItalic={true}/>
            </div>
            <div className='sign-in__agree-line'>
                <Checkbox isChecked={false} />
                <span>I agree to the Terms of Service.</span>
            </div>
            <div className='sign-in__buttons'>
                <Button buttonClass={'sign-in__button'} buttonModify={'sign-in__button_green'} buttonValue={'Sign Up'}/>
                <div className='sign-in__span'>
                    <span>or</span>
                </div>
                <Button buttonClass={'sign-in__button'} buttonValue={'Sign In'}/>
            </div>
            <div className='sign-in__have-acc'>
                <span>Do you have an Account?</span>
                <Link linkClass={'sign-in__link'} linkHref={'#sign-in'} linkValue={'Sign In'}/>
            </div>
        </div>
    );
}

export default SignIn;
