import './title.scss';

function Title({titleClass, titleText}) {
    return (
        <h2 className={`${titleClass} title`}>{titleText}</h2>
    );
}

export default Title;
