import './input.scss';

function Input({typeValue, placeholderValue, isItalic}) {
  return (
      <input className={`input ${isItalic ? 'input_italic' : ''}`} type={typeValue} placeholder={placeholderValue} />
  );
}

export default Input;
