import './banner.scss';
import Paragraph from '../paragraph/paragraph';
import Button from '../button/button';
import SignIn from '../sign-in/sign-in';

function Banner() {
    return (
        <div className='header__banner'>
            <div className='header__banner-text'>
                <h1>Search engines use links to crawl the web</h1>
                <Paragraph pClass={'p_white'} pText={1}/>
                <div>
                    <Button buttonValue={'Learn More'}/>
                </div>
            </div>
            <div className='header__banner-sign'>
                <SignIn/>
            </div>
        </div>
    );
}

export default Banner;
