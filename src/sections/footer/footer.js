import './footer.scss';
import Logo from '../../components/logo/logo';
import Link from '../../components/link/link';

function Footer() {

    return (
        <footer className='footer'>
            <div className='container'>
                <div className='footer__row'>
                    <div>
                        <Logo isDark={true}/>
                        <span>© 2022 Links4u. All rights reserved.</span>
                    </div>
                    <div className='footer__nav'>
                        <ul>
                            <li>
                                <Link linkHref={'/'} linkClass={'footer__link'} linkValue={'Home'}/>
                            </li>
                            <li>
                                <Link linkHref={'#price'} linkClass={'footer__link'} linkValue={'Price'}/>
                            </li>
                            <li>
                                <Link linkHref={'#contact_us'} linkClass={'footer__link'} linkValue={'Contact Us'}/>
                            </li>
                            <li>
                                <Link linkHref={'#sign_in'} linkClass={'footer__link'} linkValue={'Sign In'}/>
                            </li>
                            <li>
                                <Link linkHref={'#sign_up'} linkClass={'footer__link'} linkValue={'Sign Up'}/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className='footer__bottom'>
                <div className='container'>
                    <div className='footer__bottom-row'>
                        <span>Startup Framework contains components and complex blocks which can easily be integrated into almost any design. </span>
                        <div>
                            <span className='icon-twitter'></span>
                            <span className='icon-facebook'></span>
                            <span className='icon-google-plus'></span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
