import './hyperlinks.scss';
import Title from '../../components/title/title';
import Paragraph from '../../components/paragraph/paragraph';
import Button from "../../components/button/button";

function Hyperlinks() {
    return (
        <section className='hyperlinks'>
            <div className='container'>
                <div className='hyperlinks__row'>
                    <Title
                        titleClass={'hyperlinks__title'}
                        titleText={'Link building is the process of acquiring hyperlinks from other websites to your own'}
                    />
                    <div className='hyperlinks__text'>
                        <Paragraph pClass={'p_grey'} pText={2}/>
                        <Paragraph pClass={'p_grey'} pText={3}/>
                    </div>
                    <div>
                        <Button buttonValue={'Learn More >>'}/>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Hyperlinks;
