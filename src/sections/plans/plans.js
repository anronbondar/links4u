import './plans.scss';
import Title from '../../components/title/title';
import Subtitle from '../../components/subtitle/subtitle';
import PlanItem from "../../components/plan-item/plan-item";

function Plans() {

    let planItemBasic = {
        title: 'Basic',
        subtitle: 'For all individuals and starters who want to start with domain.',
        price: 19,
    };

    let planItemPro = {
        title: 'Professional',
        subtitle: 'For professional domain names investors with a big portfolio.',
        price: 49,
        popular: true,
    };

    let planItemAdvanced = {
        title: 'Advanced',
        subtitle: 'For businesses, enterprise domain registrars and registries.',
        price: 99,
    };

    return (
        <section className='plans'>
            <div className='container'>
                <div className='plans__row'>
                    <Title
                        titleClass={'plans__title'}
                        titleText={'The Right Plan for Your Business'}
                    />
                    <Subtitle
                        subtitleClass={'plans__subtitle'}
                        subtitleText={'We have several powerful plans to showcase your business and get discovered as a creative entrepreneurs. Everything you need.'}
                    />
                    <div className='plans__list'>
                        <PlanItem planItemInfo={planItemBasic} isWhite={false}/>
                        <PlanItem planItemInfo={planItemPro} isWhite={true}/>
                        <PlanItem planItemInfo={planItemAdvanced} isWhite={false}/>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Plans;
