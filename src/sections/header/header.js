import './header.scss';
import Navigation from '../../components/navigation/navigation';
import Banner from '../../components/banner/banner';

function Header() {
    return (
        <header className='header'>
            <div className='container'>
                <div className='header__row'>
                    <Navigation/>
                    <Banner/>
                </div>
            </div>
        </header>
    );
}

export default Header;
